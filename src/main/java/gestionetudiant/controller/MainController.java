package gestionetudiant.controller;


import java.awt.Color;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

import gestionetudiant.dao.ConnectDB;
import gestionetudiant.dao.CrudDB;
import gestionetudiant.entity.Etudiant;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.paint.Paint;

public class MainController implements Initializable{
	
    @FXML
    private ComboBox<String> genreValue = new ComboBox<String>();
 
    @FXML
    private Button ajouterButton;
    
    @FXML
    private Button annulerButton;
    
    @FXML
    private Label error;
    
    @FXML
    private TextField nomValue;
    
    @FXML
    private TextField prenomValue;
    
    @FXML
    private TableView<Etudiant> table;
    
    @FXML
    private TextField ageValue;
    
    @FXML
    private TextField adresseValue;
    
    private ObservableList<String> genreList = FXCollections.observableArrayList("Homme", "Fille");

    
	public void initialize(URL location, ResourceBundle resources) {
		genreValue.setItems(genreList);
		genreValue.getSelectionModel().select(0);
		
		try {
			this.refreshTab();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	@FXML
	public void handlerButtonAction(ActionEvent event) throws Exception{
		try{
			if(event.getSource().equals(annulerButton))
				throw new AnnuleException("Op�ration annul�e");
			
			String nom = nomValue.getText();
			String prenom = prenomValue.getText();
			int age = Integer.parseInt(ageValue.getText());
			String adresse = adresseValue.getText();
			String genre = genreValue.getValue();
			
			Etudiant etudiant = new Etudiant(0, nom, prenom, age, adresse, genre);
			
			CrudDB.insert(etudiant, "etudiant");

			nomValue.setText("");
			prenomValue.setText("");
			ageValue.setText("");
			adresseValue.setText("");
			genreValue.getSelectionModel().select(0);
			
			error.setTextFill(javafx.scene.paint.Color.GREEN);
			error.setText("Etudiant ajout� avec succ�s !");
			
		}catch(AnnuleException e){
			
			nomValue.setText("");
			prenomValue.setText("");
			ageValue.setText("");
			adresseValue.setText("");
			genreValue.getSelectionModel().select(0);

			error.setTextFill(javafx.scene.paint.Color.ORANGE);
			error.setText(e.getMessage());
			
		}catch(NumberFormatException ne){
			
			error.setTextFill(javafx.scene.paint.Color.RED);
			error.setText("Veuillez remplir les champs!");
			
		}
	}
	public void refreshTab() throws Exception{
		table.getItems().removeAll(table.getItems());
		
		Etudiant[] listeEtudiant = (Etudiant[]) CrudDB.select("etudiant", "gestionetudiant.entity", "", new ConnectDB("gestionetudiant"));
		ObservableList<Etudiant> data = FXCollections.observableArrayList(Arrays.asList(listeEtudiant));
		System.out.println("================================");
		System.out.println(data.get(0).getGenre());
		table.setItems(data);
		
	}
	
	class AnnuleException extends Exception{
		public AnnuleException(String msg){
			super(msg);
		}
		public AnnuleException(){
			
		}
	}
}
